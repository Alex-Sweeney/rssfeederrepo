package com.rssFeeder.MainTest;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import com.rssFeeder.models.User;
import com.rssFeeder.repositories.UserRepository;
 
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserEntityTest {
	
    @Autowired
    private TestEntityManager entityManager;
     
    @Autowired
    private UserRepository userRepo;
    
    @Test
    public void createNewUser() {
        User user = new User();
        user.setEmail("borisMallock@gmail.com");
        user.setPassword("bemmall1999");
        user.setUsername("BorisNYC_99");
        user.setDisplayName("Boris");
         
        User savedUser = userRepo.save(user);
         
        User foundUser = entityManager.find(User.class, savedUser.getId());
         
        assertThat(user.getUsername()).isEqualTo(foundUser.getUsername());
         
    }

}

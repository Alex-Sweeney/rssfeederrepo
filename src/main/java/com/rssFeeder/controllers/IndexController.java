package com.rssFeeder.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.rssFeeder.FeedRetriever;
import com.rssFeeder.models.User;
import com.rssFeeder.models.UserFeeds;
import com.rssFeeder.repositories.FeedRepository;
import com.rssFeeder.repositories.UserRepository;

@Controller
public class IndexController {
	
    @Autowired
    private UserRepository userRepo;
    
	@GetMapping("/")
	public String greeting(Model model) {
		return "index";
	}
	
	@GetMapping("/login")
	public String loginUser(Model model) {
		return "login";
	}
	
	@GetMapping("/register")
	public String registerNewUser(Model model) {
		model.addAttribute("user", new User());
		return "register";
	}
	
	@PostMapping("/register_user")
	public String registerSuccess(User user){
	    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	    String encodedPassword = passwordEncoder.encode(user.getPassword());
	    user.setPassword(encodedPassword);
	     
	    try {
	    	userRepo.save(user);
	    } catch(Exception e) {
	    	//TODO: figure out error handling for duplicate email registering
	    	return "index";
	    }
	    
	    return "register_success";
	}
	
	@GetMapping("/about")
	public String aboutPage(Model model) {
		return "about";
	}
	
	@GetMapping("/error")
	public String error(Model model) {
		return "error";
	}

}

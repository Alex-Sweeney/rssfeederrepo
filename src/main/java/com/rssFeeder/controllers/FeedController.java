package com.rssFeeder.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.rssFeeder.FeedRetriever;
import com.rssFeeder.models.User;
import com.rssFeeder.models.UserFeeds;
import com.rssFeeder.repositories.FeedRepository;
import com.rssFeeder.repositories.UserRepository;

@Controller
public class FeedController {
	
    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private FeedRepository feedRepo;

	@GetMapping("/feeds")
	public String feedDisplay(Model model) throws Exception {
		User user = userRepo.findByEmail(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		List<UserFeeds> feeds = FeedRetriever.getUserSubscribedFeeds(user);
		
		for(UserFeeds feed : feeds) {
			
		}
		
		return "feeds";
	}
	
	@PostMapping("/addFeed")
	public String addFeed(@RequestParam String xml) {
		try {
			User user = userRepo.findByEmail(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());

			UserFeeds uf = new UserFeeds(user, xml);
			feedRepo.save(uf);
			return "feeds";
		} catch(Exception e) {
			return "feeds";
		}		
	}
}

package com.rssFeeder.models;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "UserFeeds")
public class UserFeeds implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Column(name = "feed_url")
	private String feedUrl;
	
	public UserFeeds(User user, String feedUrl) {
		this.feedUrl = feedUrl;
		this.user = user;
	}

	public String getFeedUrl() {
		return feedUrl;
	}

	public void setFeedUrl(String feedUrl) {
		this.feedUrl = feedUrl;
	}
}

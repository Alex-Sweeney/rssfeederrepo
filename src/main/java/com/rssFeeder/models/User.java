package com.rssFeeder.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;
     
    @Column(nullable = false, unique = true, length = 55)
    private String email;
     
    @Column(nullable = false, length = 128)
    private String password;
     
    @Column(name = "username", nullable = false, length = 20)
    private String username;
    
    @Column(name = "display_name", nullable = false, length = 20)
    private String displayName;

	public Long getId() {
		return user_id;
	}

	public void setId(Long id) {
		this.user_id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}

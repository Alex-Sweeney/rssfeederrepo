package com.rssFeeder.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rssFeeder.models.User;
import com.rssFeeder.models.UserFeeds;

@Repository
public interface FeedRepository extends JpaRepository<UserFeeds, Long>{
	
	@Query("SELECT uf FROM UserFeeds uf WHERE uf.user = ?1")
	List<UserFeeds> findAllByUser(User user);
	
}

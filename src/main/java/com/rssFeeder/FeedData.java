package com.rssFeeder;

import java.util.List;

public class FeedData {
    private final String feedUrl;

    public FeedData(String feedUrl) {
		this.feedUrl = feedUrl;
    }
    public String getFeedUrl() {
    	return feedUrl;
    }

}

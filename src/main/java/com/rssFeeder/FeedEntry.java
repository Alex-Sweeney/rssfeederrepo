package com.rssFeeder;

import java.util.Date;

import com.rometools.rome.feed.synd.SyndEntry;

public class FeedEntry {	
	private String title;

	private String link;
	
	private Date publishedDate;
	
	public FeedEntry(String title, String link, Date publishedDate) {
		this.title = title;
		this.link = link;
		this.publishedDate = publishedDate;
    }

	public static FeedEntry fromSyndEntry(SyndEntry feedEntry) {
		return new FeedEntry(feedEntry.getTitle(), feedEntry.getLink(), feedEntry.getPublishedDate());
    }	
	
	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public Date getPublishedDate() {
		return publishedDate;
	}
}

package com.rssFeeder;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.rometools.fetcher.*;
import com.rometools.fetcher.impl.*;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rssFeeder.models.User;
import com.rssFeeder.models.UserFeeds;
import com.rssFeeder.repositories.FeedRepository;

public class FeedRetriever {
	
	@Autowired
	private static FeedRepository feedRepo;
	
	private static FeedFetcherCache feedInfoCache = HashMapFeedInfoCache.getInstance();
	
	public static List<UserFeeds> getUserSubscribedFeeds(User user) {
		try{
			return feedRepo.findAllByUser(user);
		} catch(Exception e) {
			//no feeds found
			return new ArrayList<UserFeeds>();
		}
	}
	
	public static List<FeedEntry> getEntriesByFeed(String feedXml) throws Exception{
		FeedFetcher feedFetcher = new HttpURLFeedFetcher(feedInfoCache);
		//feed.getFeedUrl()
		SyndFeed feedFound = feedFetcher.retrieveFeed(new URL(feedXml));
		return null;
	}
}
